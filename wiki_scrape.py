import requests
from collections import defaultdict
from bs4 import BeautifulSoup
from sklearn.decomposition import PCA
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer
import re
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
from numpy.linalg import norm
from random import sample
import pickle
import os.path
import seaborn as sns
import pandas as pd
from time import sleep

def get_text(html):
	text = ''
	soup = BeautifulSoup(html)
	for p in soup.findAll('p'):
		t = re.sub(r'\[\d+\]', ' ', p.text)
		text += t
	return text

def get_links(html):
	links = []
	soup = BeautifulSoup(html)
	for p in soup.findAll('p'):
		for a in p.findAll('a'):
			#print 'a:', a
			href = a['href']
			if href[:6] == '/wiki/':
				link = href.split('/')[-1]
				links.append(link)
	return links

def get_all_parents(suffix, parentdict):
	pars = list(suffix)
	while parentdict[suffix] is not suffix:
		par = parentdict[suffix]
		suffix = par
	return par

def add_parent(suffix, parentdict, seeds):
	pars = parentdict[suffix]
	#for p
	parentdict[suffix].add(par)

def scrape_corpus(seeds):
	base_url = 'http://en.wikipedia.org/wiki/'
	max_depth = 1
	sample_max = 200
	queue = seeds
	visited = defaultdict(str)
	parent = defaultdict(set)
	for seed in queue:
		parent[seed] = set([seed])
	#print 'parent:', parent
	current_depth = 0
	#asdf = '''
	#print queue
	while current_depth <= max_depth:
		q = []
		print 'len(queue):', len(queue)
		print 'current_depth:', current_depth
		for i, suffix in enumerate(sample(queue, min(len(queue), sample_max))):
		#for i, suffix in enumerate(queue):
			if suffix in visited:
				continue
			else:
				if i % 10 == 0:
					print i, 'of', min(len(queue), sample_max)
				try:
					print 'visiting', suffix, parent[suffix]
					url = base_url+suffix
					r = requests.get(url)
					text = get_text(r.content)
					new_suffixes = get_links(r.content)
					#print 'new_suffixes:', new_suffixes
					q += [s for s in new_suffixes if s not in visited]
					q = list(set(q))
					#print 'visited', suffix
					visited[suffix] = text
					for s in new_suffixes:
						#if s not in visited:
							parent[s] = parent[s].union(parent[suffix])
							#print s, parent[s]
						#else:
						#add_parent(suffix, parent, seeds)
						 #   print suffix, 'visited in', get_parent_suffix(suffix, parent)
					#print len(visited)
					sleep(1)
				except Exception as e:
					print e
		#else:
		 #   pass
		print 'len(visited):', len(visited)
		queue = q[:]
		current_depth += 1
	corpus = [(parent[t[0]], t[1]) for t in visited.iteritems()]
	print 'len(corpus):', len(corpus)
	return corpus

def trans(corpus, n_dim = 2):
	vectorizer = TfidfVectorizer(stop_words = 'english')
	docs = [e[1] for e in corpus]
	X = vectorizer.fit_transform(docs)
	pca = PCA(n_components=n_dim)
	X = pca.fit_transform(X.toarray())
	return X

def get_parents_list(corpus):
	res = []
	for parents, doc in corpus:
		#print parents
		p = ' '.join([p for p in parents])
		#print 'before:', p
		p = p.replace('-', '_')
		#print 'after:', p
		res.append(p)
	cv = CountVectorizer()
	res = cv.fit_transform(res).todense()
	#print res
	df = pd.DataFrame(res, columns=cv.vocabulary_)
	return df

def plot_correlation_matrix(data):
	cmap = sns.diverging_palette(220, 10, as_cmap=True)
	f, ax = plt.subplots()#figsize=(10,10))
	ax.set_title('correlation between topics')
	sns.corrplot(data, annot=True, diag_names=False, cmap=cmap)
	#f.tight_layout()
	plt.show()

def plot(seeds, corpus, X):
	sns.set_style("whitegrid", {'legend.frameon':True})
	cmaps = ['cool', 'autumn', 'winter', 'summer', 'spring', 'jet']
	cmaps = [plt.cm.cool, plt.cm.autumn, plt.cm.winter,
		 plt.cm.summer, plt.cm.spring, plt.cm.jet]
	#suffixes = ['Multi-armed_bandit', 'Machine_learning', 'Statistics', 
	#	'Computer_science', 'Probability_theory', 'Modern_art']
	cmaps = [plt.cm.Blues, plt.cm.Greens, plt.cm.Greys,
		 plt.cm.Oranges, plt.cm.Purples, plt.cm.Reds]
	f, ax = plt.subplots(1,1,figsize=(10,5))
	ax.axis('off')
	ax.set_title('Venn Diagram')
	#ax.ticks('off')
	labels = []
	handles = []
	to_view = [0,2]#range(len(seeds))#
	for i in range(len(seeds)):
		if i not in to_view:
			continue
		inds = []
		topic = list(seeds)[i]
		#print topic, cmaps[i]
		labels.append(topic)
		for j, doc in enumerate(corpus):
			if topic in doc[0]:
				inds.append(j)
		if len(inds) <= 2:
			continue
		subset = X[inds]
		print topic, 'subsets:', len(subset)
		#print subset[:,0]
		df = pd.DataFrame(subset, columns=['x', 'y'])
		h = sns.kdeplot(df.x, df.y, shade=False, legend=True, label=topic, cmap=cmaps[i], ax=ax)
		#sns.kdeplot(subset[:,0], subset[:,1], shade=False, label=topic, cmap=cmaps[i])
		#handles.append(h)
	#print handles
	#print labels
	patches = []
	for i in [i for i in range(len(seeds)) if i in to_view]:
		pat = mpatches.Patch(facecolor=cmaps[i](0.5), label=seeds[i])
		patches.append(pat)
	f.patch.set_facecolor('white')
	plt.legend(handles=patches)
	fname = ''.join([seeds[i] for i in to_view])+'.png'
	plt.savefig(fname)
	print 'see', fname
	#plt.show()

if __name__ == '__main__':
	fname = 'wiki_corpus'
	seeds = ['Multi-armed_bandit', 'Machine_learning', 'Statistics', 
		'Computer_science', 'Probability_theory', 'Modern_art']
	if os.path.isfile(fname):
		corpus = pickle.load(open(fname, 'r'))
		print 'found it!'
	else:
		print 'scraping...'
		corpus = scrape_corpus(seeds)
		pickle.dump(c, open(fname, 'w'))
	pars = get_parents_list(corpus)
	#print pars.columns
	#plot_correlation_matrix(pars)
	X = trans(corpus)
	plot(seeds, corpus, X)
